package com.example.tesisproyecto;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.heatmaps.WeightedLatLng;

import java.util.ArrayList;

public class FrmPreTriaje extends AppCompatActivity {
DrawerLayout drawerLayout;
    RadioButton preg1S, preg1N,preg2S, preg2N,preg3S, preg3N,preg4S, preg4N,preg5S, preg5N,preg6S, preg6N,preg7S, preg7N,preg8S, preg8N,preg9S, preg9N,preg10S, preg10N,preg11S, preg11N
            ,preg12S, preg12N;
    int respP, respN;
    String respG1, respG2,respG3,respG4,respG5,respG6,respG7,respG8,respG9,respG10,respG11,respG12;
    String respuesta;
    String idUser;
    public static final int REQUEST_CODE = 1;
    FusedLocationProviderClient fusedLocationProviderClient;
    String lat, lon;

    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReferenceFromUrl("https://tesis-c2eb2-default-rtdb.firebaseio.com/");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frm_pre_triaje);
        getSupportActionBar().hide();
        drawerLayout = findViewById(R.id.drawer_activity);
        preg1S = findViewById(R.id.preg1S);
        preg1N = findViewById(R.id.preg1N);
        preg2S = findViewById(R.id.preg2S);
        preg2N = findViewById(R.id.preg2N);
        preg3S = findViewById(R.id.preg3S);
        preg3N = findViewById(R.id.preg3N);
        preg4S = findViewById(R.id.preg4S);
        preg4N = findViewById(R.id.preg4N);
        preg5S = findViewById(R.id.preg5S);
        preg5N = findViewById(R.id.preg5N);
        preg6S = findViewById(R.id.preg6S);
        preg6N = findViewById(R.id.preg6N);
        preg7S = findViewById(R.id.preg7S);
        preg7N = findViewById(R.id.preg7N);
        preg8S = findViewById(R.id.preg8S);
        preg8N = findViewById(R.id.preg8N);
        preg9S = findViewById(R.id.preg9S);
        preg9N = findViewById(R.id.preg9N);
        preg10S = findViewById(R.id.preg10S);
        preg10N = findViewById(R.id.preg10N);
        preg11S = findViewById(R.id.preg11S);
        preg11N = findViewById(R.id.preg11N);
        preg12S = findViewById(R.id.preg12S);
        preg12N = findViewById(R.id.preg12N);
        idUser = getIntent().getStringExtra("idUser");
    }
    public void ClickMenu(View v){
        Mapa.openDrawer(drawerLayout);
    }
    public void ClickLogo(View v){
        Mapa.closeDrawer(drawerLayout);
    }
    public void Clickhome(View v){
        Mapa.redirectActivity(this, Mapa.class);
    }
    public void clickLineAyuda(View v){
        Mapa.redirectActivity(this,LineaAyuda.class);
    }
    public void clickTriaje(View v){
        recreate();
    }
    public void clickReporte(View v){
        Mapa.redirectActivity(this, Reporte.class);
    }
    public void clickLogout(View v){
        Mapa.logout(this);
    }
    @Override
    protected void onPause(){
        super.onPause();
        Mapa.closeDrawer(drawerLayout);
    }

    public void ObtenerCoordendasActual() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(FrmPreTriaje.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_CODE);
        } else {

            getCoordenada();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getCoordenada();
            } else {
                Toast.makeText(this, "Permiso Denegado ..", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void getCoordenada() {

        try {
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setInterval(10000);
            locationRequest.setFastestInterval(3000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(locationRequest, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    LocationServices.getFusedLocationProviderClient(FrmPreTriaje.this).removeLocationUpdates(this);
                    if (locationResult != null && locationResult.getLocations().size() > 0) {
                        int latestLocationIndex = locationResult.getLocations().size() - 1;
                        double latitud = locationResult.getLocations().get(latestLocationIndex).getLatitude();
                        double longitude = locationResult.getLocations().get(latestLocationIndex).getLongitude();
                        lat=String.valueOf(latitud);
                        lon=String.valueOf(longitude);
                        //guardar longitud y latitud
                        databaseReference.child("district").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                //ingresando data a la base de datos
                                databaseReference.child("district").child(idUser).child("density").setValue(213);
                                databaseReference.child("district").child(idUser).child("district").setValue("user");
                                databaseReference.child("district").child(idUser).child("hq").setValue("user");
                                databaseReference.child("district").child(idUser).child("lat").setValue(lat);
                                databaseReference.child("district").child(idUser).child("lon").setValue(lon);
                                Toast.makeText(FrmPreTriaje.this, "Formulario guardado con exito", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(FrmPreTriaje.this, Mapa.class);
                                intent.putExtra("idUser", idUser);
                                startActivity(intent);
                            }


                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                    }

                }

            }, Looper.myLooper());

        }catch (Exception ex){
            System.out.println("Error es :" + ex);
        }

    }
    public void GuardarTriaje(View v){

        //validar si la primera pregunta esta marcada
        if((preg1S.isChecked() || preg1N.isChecked())){
            if(preg1S.isChecked()){
                respP = respP + 1;
            }else{
                respN = respN + 1;
            }
            //validar si la pregunta 2 esta marcada
            if((preg2S.isChecked() || preg2N.isChecked())){
                if(preg2S.isChecked()){
                    respP = respP + 1;
                    respG1 = "si";
                }else{
                    respN = respN + 1;
                    respG1 = "No";
                }
                //validar si la pregunta 3 esta marcada
                if((preg3S.isChecked() || preg3N.isChecked())){
                    if(preg3S.isChecked()){
                        respP = respP + 1;
                        respG2 = "si";
                    }else{
                        respN = respN + 1;
                        respG2 = "no";
                    }
                    //validar si la pregunta 4 esta marcada
                    if((preg4S.isChecked() || preg4N.isChecked())){
                        if(preg4S.isChecked()){
                            respP = respP + 1;
                        }else{
                            respN = respN + 1;
                        }
                        //validar si la pregunta 5 esta marcada
                        if((preg5S.isChecked() || preg5N.isChecked())){
                            if(preg5S.isChecked()){
                                respP = respP + 1;
                                respG5 = "si";
                            }else{
                                respN = respN + 1;
                                respG5 = "no";
                            }
                            //validar si la pregunta 6 esta marcada
                            if((preg6S.isChecked() || preg6N.isChecked())){
                                if(preg6S.isChecked()){
                                    respP = respP + 1;
                                    respG6 = "si";
                                }else{
                                    respN = respN + 1;
                                    respG6 = "no";
                                }
                                //validar si la pregunta 7 esta marcada
                                if((preg7S.isChecked() || preg7N.isChecked())){
                                    if(preg7S.isChecked()){
                                        respP = respP + 1;
                                        respG7 = "si";
                                    }else{
                                        respN = respN + 1;
                                        respG7 = "no";
                                    }
                                    //validar si la pregunta 8 esta marcada
                                    if((preg8S.isChecked() || preg8N.isChecked())){
                                        if(preg8S.isChecked()){
                                            respP = respP + 1;
                                            respG8 = "si";
                                        }else{
                                            respN = respN + 1;
                                            respG8 = "no";
                                        }
                                        //validar si la pregunta 9 esta marcada
                                        if((preg9S.isChecked() || preg9N.isChecked())){
                                            if(preg9S.isChecked()){
                                                respP = respP + 1;
                                                respG9 = "si";
                                            }else{
                                                respN = respN + 1;
                                                respG9 = "no";
                                            }
                                            //validar si la pregunta 10 esta marcada
                                            if((preg10S.isChecked() || preg10N.isChecked())){
                                                if(preg10S.isChecked()){
                                                    respP = respP + 1;
                                                    respG10 = "si";
                                                }else{
                                                    respN = respN + 1;
                                                    respG10 = "no";
                                                }
                                                //validar si la pregunta 11 esta marcada
                                                if((preg11S.isChecked() || preg11N.isChecked())){
                                                    if(preg11S.isChecked()){
                                                        respP = respP + 1;
                                                        respG11 = "si";
                                                    }else{
                                                        respN = respN + 1;
                                                        respG12 = "no";
                                                    }
                                                    //validar si la pregunta 12 esta marcada
                                                    if((preg12S.isChecked() || preg12N.isChecked())){
                                                        if(preg12S.isChecked()){
                                                            respP = respP + 1;
                                                            respG12 = "si";
                                                        }else{
                                                            respN = respN + 1;
                                                            respG12 = "no";
                                                        }
                                                        //validar si es un caso posible de covid
                                                        if(respP>=11){
                                                            respuesta = "Estos son los resulatodos de la evaluación\nPosible caso de contagio"+
                                                                    "\nSi le da en el boton de no volvera a registrar el formulario" +
                                                                    "\nSi acepta guardaremos sus coordenadas para poder hacerle seguimiento";
                                                        }else{
                                                            respuesta = "Estos son los resulatodos de la evaluación\nCaso negativo de Covid"+
                                                                    "\nSi le da en el boton de no volvera a registrar el formulario" +
                                                                    "\nSi acepta sera redireccionado a la vista principal";
                                                        }
                                                        AlertDialog.Builder builder = new AlertDialog.Builder(FrmPreTriaje.this);
                                                        builder.setTitle("Resultados");
                                                        builder.setMessage(respuesta);
                                                        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                databaseReference.child("Pretriaje").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                                        //ingresando data a la base de datos
                                                                        databaseReference.child("Pretriaje").child(idUser).child("Pregunta1").setValue(respG1);
                                                                        databaseReference.child("Pretriaje").child(idUser).child("Pregunta2").setValue(respG2);
                                                                        databaseReference.child("Pretriaje").child(idUser).child("Pregunta3").setValue(respG3);
                                                                        databaseReference.child("Pretriaje").child(idUser).child("Pregunta4").setValue(respG4);
                                                                        databaseReference.child("Pretriaje").child(idUser).child("Pregunta5").setValue(respG5);
                                                                        databaseReference.child("Pretriaje").child(idUser).child("Pregunta6").setValue(respG6);
                                                                        databaseReference.child("Pretriaje").child(idUser).child("Pregunta7").setValue(respG7);
                                                                        databaseReference.child("Pretriaje").child(idUser).child("Pregunta8").setValue(respG8);
                                                                        databaseReference.child("Pretriaje").child(idUser).child("Pregunta9").setValue(respG9);
                                                                        databaseReference.child("Pretriaje").child(idUser).child("Pregunta10").setValue(respG10);
                                                                        databaseReference.child("Pretriaje").child(idUser).child("Pregunta11").setValue(respG11);
                                                                        databaseReference.child("Pretriaje").child(idUser).child("Pregunta12").setValue(respG12);

                                                                        ObtenerCoordendasActual();
                                                                    }


                                                                    @Override
                                                                    public void onCancelled(@NonNull DatabaseError error) {

                                                                    }
                                                                });

                                                            }
                                                        });
                                                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                finish(); startActivity(getIntent());
                                                            }
                                                        });
                                                        builder.show();
                                                    }else{
                                                        Toast.makeText(FrmPreTriaje.this, "no se marco la pregunta 12", Toast.LENGTH_SHORT).show();
                                                    }
                                                }else{
                                                    Toast.makeText(FrmPreTriaje.this, "no se marco la pregunta 11", Toast.LENGTH_SHORT).show();
                                                }
                                            }else{
                                                Toast.makeText(FrmPreTriaje.this, "no se marco la pregunta 10", Toast.LENGTH_SHORT).show();
                                            }
                                        }else{
                                            Toast.makeText(FrmPreTriaje.this, "no se marco la pregunta 9", Toast.LENGTH_SHORT).show();
                                        }
                                    }else{
                                        Toast.makeText(FrmPreTriaje.this, "no se marco la pregunta 8", Toast.LENGTH_SHORT).show();
                                    }
                                }else{
                                    Toast.makeText(FrmPreTriaje.this, "no se marco la pregunta 7", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(FrmPreTriaje.this, "no se marco la pregunta 6", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(FrmPreTriaje.this, "no se marco la pregunta 5", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(FrmPreTriaje.this, "no se marco la pregunta 4", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(FrmPreTriaje.this, "no se marco la pregunta 3", Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(FrmPreTriaje.this, "no se marco la pregunta 2", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(FrmPreTriaje.this, "no se marco la pregunta 1", Toast.LENGTH_SHORT).show();
        }
    }

}