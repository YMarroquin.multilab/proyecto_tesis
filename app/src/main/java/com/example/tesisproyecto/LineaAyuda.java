package com.example.tesisproyecto;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.firebase.database.core.Repo;

public class LineaAyuda extends AppCompatActivity {
TextView txtwsp;
DrawerLayout drawerLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linea_ayuda);
        getSupportActionBar().hide();
        drawerLayout = findViewById(R.id.drawer_layout);

    }
    public void ClickMenu(View v){
        Mapa.openDrawer(drawerLayout);
    }
    public void ClickLogo(View v){
        Mapa.closeDrawer(drawerLayout);
    }
    public void Clickhome(View v){
        Mapa.redirectActivity(this, Mapa.class);
    }
    public void clickLineAyuda(View v){
        recreate();
    }
    public void clickTriaje(View v){
        Mapa.redirectActivity(this,FrmPreTriaje.class);
    }
    public void clickReporte(View v){
        Mapa.redirectActivity(this, Reporte.class);
    }
    public void clickLogout(View v){
        Mapa.logout(this);
    }
    @Override
    protected void onPause(){
        super.onPause();
        Mapa.closeDrawer(drawerLayout);
    }
}