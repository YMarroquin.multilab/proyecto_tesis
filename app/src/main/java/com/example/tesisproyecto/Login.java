package com.example.tesisproyecto;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class Login extends AppCompatActivity {
TextView olvidasteContra, nuevoUsuario,Terminos;
TextInputEditText correoLogin, contraLogin;
Button btn;

private FirebaseAuth mauth;
    //crear referencia a database Real time
    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReferenceFromUrl("https://tesis-c2eb2-default-rtdb.firebaseio.com/");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        correoLogin = findViewById(R.id.celularLogin);
        contraLogin = findViewById(R.id.contraLogin);

        btn = findViewById(R.id.InicioSesion);

        mauth = FirebaseAuth.getInstance();

        olvidasteContra = findViewById(R.id.olvidasteContra);
        nuevoUsuario = findViewById(R.id.nuevoUsuario);
        Terminos = findViewById(R.id.Terminos);
        olvidasteContra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, RecuperarClave.class);
                startActivity(intent);
            }
        });
        nuevoUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, Registrar.class);
                startActivity(intent);
            }
        });
        Terminos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, Terminos.class);
                startActivity(intent);
            }
        });
    }

    public void iniciarSesion(View v){
        String correo = correoLogin.getText().toString();
        String password = contraLogin.getText().toString();

        if(correo.isEmpty()  || password.isEmpty()){
            Toast.makeText(Login.this, "Por favor ingrese su correo o contraseña", Toast.LENGTH_SHORT).show();
        }else{
            if(!Patterns.EMAIL_ADDRESS.matcher(correo).matches()){
                Toast.makeText(Login.this, "Correo invalido", Toast.LENGTH_SHORT).show();
            }else{
                mauth.signInWithEmailAndPassword(correo, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Intent intent = new Intent(Login.this,Mapa.class);
                            startActivity(intent);
                            finish();
                        }else{
                            Toast.makeText(Login.this, "Credenciales equivocadas, trata de nuevo", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }

        }
    }

    @Override
    public void onBackPressed() {}
}