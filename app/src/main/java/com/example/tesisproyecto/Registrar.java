package com.example.tesisproyecto;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Registrar extends AppCompatActivity {
    TextInputEditText usuarioRg, correoEditRg,numeroEditRg,direccionEditRg,edadEditRg,contraRegi;
    RadioButton acptarTerminos, sexoM, sexoF, otro;
    Button registrar;
    String sexo;

    FirebaseAuth mauth;

    //crear referencia a database Real time
    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReferenceFromUrl("https://tesis-c2eb2-default-rtdb.firebaseio.com/");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);
        getSupportActionBar().hide();
        usuarioRg = findViewById(R.id.nombresRg);
        correoEditRg = findViewById(R.id.correoEditRg);
        numeroEditRg = findViewById(R.id.numeroEditRg);
        direccionEditRg = findViewById(R.id.direccionEditRg);
        edadEditRg = findViewById(R.id.edadEditRg);
        contraRegi = findViewById(R.id.contraRegi);

        sexoM = findViewById(R.id.sexoM);
        sexoF = findViewById(R.id.sexoF);
        otro = findViewById(R.id.sexoOtro);

        acptarTerminos = findViewById(R.id.acptarTerminos);

        mauth = FirebaseAuth.getInstance();

        registrar = findViewById(R.id.registrar);
        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre = usuarioRg.getText().toString();
                String correo = correoEditRg.getText().toString();
                String celular = numeroEditRg.getText().toString();
                String direccion = direccionEditRg.getText().toString();
                String edad = edadEditRg.getText().toString();
                String contra = contraRegi.getText().toString();

                if(nombre.isEmpty() || correo.isEmpty() || celular.isEmpty() || direccion.isEmpty() || edad.isEmpty() || contra.isEmpty()){
                    Toast.makeText(Registrar.this, "Por favor, complete todos los campos", Toast.LENGTH_SHORT).show();
                }else{
                    if(sexoM.isChecked()){
                        sexo = "M";
                    }else if(sexoF.isChecked()){
                        sexo = "F";
                    }else if(otro.isChecked()){
                        sexo = "otro";
                    }else{
                        Toast.makeText(Registrar.this, "Por favor, complete todos los campos", Toast.LENGTH_SHORT).show();
                    }
                    if(sexo.length() > 0){
                        if(!acptarTerminos.isChecked()){
                            Toast.makeText(Registrar.this, "Por favor, acepte los terminos", Toast.LENGTH_SHORT).show();
                        }else{

                            if(contra.length() >= 6){
                                mauth.createUserWithEmailAndPassword(correo, contra).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()){
                                            String id = mauth.getCurrentUser().getUid();
                                            databaseReference.child("User").addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                                    //revisa si el correo esta registrado
                                                    if(snapshot.hasChild(id)){
                                                        Toast.makeText(Registrar.this, "el correo ya se encuentra registrado", Toast.LENGTH_SHORT).show();
                                                    }else{
                                                        //ingresando data a la base de datos
                                                        databaseReference.child("User").child(id).child("Nombres completos").setValue(nombre);
                                                        databaseReference.child("User").child(id).child("celular").setValue(celular);
                                                        databaseReference.child("User").child(id).child("direccion").setValue(direccion);
                                                        databaseReference.child("User").child(id).child("edad").setValue(edad);
                                                        databaseReference.child("User").child(id).child("sexo").setValue(sexo);

                                                        Toast.makeText(Registrar.this, "Usuario registrado con exito", Toast.LENGTH_SHORT).show();
                                                        Intent intent = new Intent(Registrar.this, PreTriaje.class);
                                                        intent.putExtra("idUser", id);
                                                        startActivity(intent);
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError error) {

                                                }
                                            });
                                        }else{
                                            Toast.makeText(Registrar.this, "No se pudo registrar este usuario", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            }else{
                                Toast.makeText(Registrar.this, "La contraseña debe tener mas de 6 caracteres", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                }
            }
        });
    }
}