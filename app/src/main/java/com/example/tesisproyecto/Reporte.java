package com.example.tesisproyecto;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.google.maps.android.heatmaps.WeightedLatLng;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Reporte extends AppCompatActivity {
DrawerLayout drawerLayout;
    PieChart pieChart;
    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReferenceFromUrl("https://tesis-c2eb2-default-rtdb.firebaseio.com/");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte);
        getSupportActionBar().hide();
        drawerLayout = findViewById(R.id.drawer_reporte);
        pieChart= findViewById(R.id.torta);
        grafico();
    }
    public void ClickMenu(View v){
        Mapa.openDrawer(drawerLayout);
    }
    public void ClickLogo(View v){
        Mapa.closeDrawer(drawerLayout);
    }
    public void Clickhome(View v){
        Mapa.redirectActivity(this, Mapa.class);
    }
    public void clickLineAyuda(View v){
        Mapa.redirectActivity(this, LineaAyuda.class);
    }
    public void clickTriaje(View v){
        Mapa.redirectActivity(this,FrmPreTriaje.class);
    }
    public void clickReporte(View v){
        recreate();
    }
    public void clickLogout(View v){
        Mapa.logout(this);
    }
    @Override
    protected void onPause(){
        super.onPause();
        Mapa.closeDrawer(drawerLayout);
    }
    public void grafico(){
        ArrayList data = new ArrayList();
        databaseReference.child("info").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<PieEntry> entries = new ArrayList<>();
                if(dataSnapshot.exists()){
                    for (DataSnapshot ds: dataSnapshot.getChildren()) {
                        float cantidad = Float.valueOf((ds.child("cantidad").getValue().toString()));
                        String nombre = ds.child("nombre").getValue().toString();
                        entries.add(new PieEntry(cantidad, nombre));

                        PieDataSet set = new PieDataSet(entries, "Reporte de los contagios");
                        PieData data = new PieData(set);
                        pieChart.setData(data);
                        pieChart.invalidate(); // refresh

                        set.setColors(ColorTemplate.COLORFUL_COLORS);
                        int colorWhite = Color.parseColor("#000000");
                        set.setValueTextColor(colorWhite);
                        Legend legend = pieChart.getLegend();;
                        legend.setTextColor(colorWhite);
                        legend.setTextSize(18);
                        pieChart.setCenterTextSize(1);
                        set.setValueTextSize(10);

                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Toast.makeText(Reporte.this, (CharSequence) databaseError.toException(), Toast.LENGTH_SHORT).show();
                // ...
            }

        });
    }
}